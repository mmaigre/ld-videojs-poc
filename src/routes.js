import AppRecorder from '@/components/AppRecorder';
import AppPlayer from '@/components/AppPlayer';


const routes = [
    {
        path: '/recorder',
        name: 'Recorder',
        component: AppRecorder
    },
    {
        path: '/player',
        name: 'player',
        component: AppPlayer
    },
];

export default routes;
